﻿using System;

namespace cz._2_figura
{
    class Program
    {
        static void Main(string[] args)
        {

            String f1 = "0-0-3-0-3-3-0-3-0-0";
           // String f2 = "11-11-14-11-14-14-11-14-11-11";
            String f2 = "1-1-4-1-4-4-1-4-1-1";

            Figura figura1 = new Figura(f1);
            Console.WriteLine("figura f1: " + figura1.ToString());
            Figura figura2 = new Figura(f2);
            Console.WriteLine("figura f2: " + figura2.ToString());

            if (figura1.czyDwieFigurySaProste(figura1, figura2))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Dwie figury są proste");
                if (figura1.czyDwieFigurySiePrzecinaja(figura1, figura2))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Figury się przecinają");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Figury się nie przecinają");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Przynajmniej jedna z figur nie jest prosta");
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.ReadKey();
        }
    }
}
