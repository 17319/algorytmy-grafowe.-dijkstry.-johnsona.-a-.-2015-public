﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace cz._2_figura
{
    class Figura
    {
        List<Punkt> listaWierzcholkow;
        List<Krawedz> listaKrawedzi;

        internal List<Krawedz> ListaKrawedzi
        {
            get
            {
                return listaKrawedzi;
            }

            set
            {
                listaKrawedzi = value;
            }
        }

        public Figura(string stringZawierajacyListePunktow)
        {
            listaWierzcholkow = new List<Punkt>();
            List<int> listaZawierajacaNumeryWierzcholkow = new List<int>();
            foreach (string str in stringZawierajacyListePunktow.Split('-'))
                listaZawierajacaNumeryWierzcholkow.Add(int.Parse(str));
            for (int i = 0; i + 1 < listaZawierajacaNumeryWierzcholkow.Count;)
                listaWierzcholkow.Add(new Punkt(listaZawierajacaNumeryWierzcholkow[i++], listaZawierajacaNumeryWierzcholkow[i++]));
            ListaKrawedzi = new List<Krawedz>();
            for (int i = 0; i < listaWierzcholkow.Count - 1; i++)
                ListaKrawedzi.Add(new Krawedz(listaWierzcholkow[i], listaWierzcholkow[i + 1]));
        }

        public override string ToString()
        {
            string str = "";
            foreach (Punkt p in listaWierzcholkow)
                str += "(" + p.X + "," + p.Y + ")\n";
            return str;
        }


        public bool czyWielokatJestProsty()
        {
            Console.WriteLine("Sprawdzam czy liczba wierzchołków nie jest mniejsza od czterech, oraz czy figura zaczyna się i kończy w tym samym punkcie.");
            if (listaWierzcholkow.Count < 4 || (listaWierzcholkow.First().X != listaWierzcholkow.Last().X || listaWierzcholkow.First().Y != listaWierzcholkow.Last().Y))
                return false;

            List<Krawedz> listaKrawedzi = new List<Krawedz>();
            for (int i = 0; i < listaWierzcholkow.Count - 2; i++)
            {
                listaKrawedzi.Add(new Krawedz(listaWierzcholkow[i], listaWierzcholkow[i + 1]));
                for (int j = 0; j < listaKrawedzi.Count - 2; j++)
                {
                    Console.WriteLine("Spradzam czy krawedzie się przecinają.");
                    if (czySieStykaja(listaKrawedzi[j], listaKrawedzi.Last()))
                        return false;
                }
                Console.WriteLine("Sprawdzam czy krawędzi jest więcej niż jedna, oraz czy krawedzie sie nie nakładają, lub nie mają części wspólnej");
                if (listaKrawedzi.Count > 1 && SprawdzLacznoscKrawedzi(listaKrawedzi[listaKrawedzi.Count - 2], listaKrawedzi.Last()) == 0)
                    return false;
            }

            listaKrawedzi.Add(new Krawedz(listaWierzcholkow.First(), listaWierzcholkow[listaWierzcholkow.Count - 2]));
            for (int j = 1; j < listaKrawedzi.Count - 2; j++)
            {
                Console.WriteLine("Badania styczności krawedzi");
                if (czySieStykaja(listaKrawedzi[j], listaKrawedzi.Last()))
                    return false;
            }
            Console.WriteLine("Sprawdzam czy krawędzie przedostatnia łączy się z ostatnią, lub pierwsza łączy się z ostatnią");
            if (SprawdzLacznoscKrawedzi(listaKrawedzi[listaKrawedzi.Count - 2], listaKrawedzi.Last()) == 0 || SprawdzLacznoscKrawedzi(listaKrawedzi.First(), listaKrawedzi.Last()) == 0)
                return false;

            return true;
        }

        public static bool czySieStykaja(Krawedz kraw1, Krawedz kraw2)
        {
            int lewaGranica = 0, prawaGranica = 0, dolnaGranica = 0, gornaGranica = 0;
            int w = SprawdzLacznoscKrawedzi(kraw1, kraw2);
            int wx = Wx(kraw1, kraw2);
            if (w != 0)
            {
                if (kraw1.DajLewaGranice > kraw2.DajLewaGranice)
                    lewaGranica = kraw1.DajLewaGranice;
                else
                    lewaGranica = kraw2.DajLewaGranice;

                if (kraw1.DajPrawaGranice < kraw2.DajPrawaGranice)
                    prawaGranica = kraw1.DajPrawaGranice;
                else
                    prawaGranica = kraw2.DajPrawaGranice;

                if (lewaGranica > prawaGranica)
                    return false;


                if (kraw1.DajDolnaGranice > kraw2.DajDolnaGranice)
                    dolnaGranica = kraw1.DajDolnaGranice;
                else
                    dolnaGranica = kraw2.DajDolnaGranice;

                if (kraw1.DajGornaGranice < kraw2.DajGornaGranice)
                    gornaGranica = kraw1.DajGornaGranice;
                else
                    gornaGranica = kraw2.DajGornaGranice;

                if (dolnaGranica > gornaGranica) return false;

                float px = wx / (float)w;
                float py = Wy(kraw1, kraw2) / (float)w;
                if (px < lewaGranica || px > prawaGranica || py < dolnaGranica || py > gornaGranica)
                    return false;

                else return true;
            }
            else if (wx == 0) return false;
            else if (kraw1.DajGornaGranice < kraw2.DajDolnaGranice || kraw1.DajDolnaGranice > kraw2.DajGornaGranice)
                return false;

            return true;
        }
        private static int SprawdzLacznoscKrawedzi(Krawedz kraw1, Krawedz kraw2)
        {
            return (kraw1.P2.Y - kraw1.P1.Y) * (kraw2.P1.X - kraw2.P2.X) - (kraw2.P2.Y - kraw2.P1.Y) * (kraw1.P1.X - kraw1.P2.X);
        }
        private static int Wx(Krawedz kraw1, Krawedz kraw2)
        {
            return (kraw1.P1.X - kraw1.P2.X) * (kraw2.P2.X * kraw2.P1.Y - kraw2.P1.X * kraw2.P2.Y) - (kraw2.P1.X - kraw2.P2.X) * (kraw1.P2.X * kraw1.P1.Y - kraw1.P1.X * kraw1.P2.Y);
        }
        private static int Wy(Krawedz kraw1, Krawedz kraw2)
        {
            return (kraw1.P2.X * kraw1.P1.Y - kraw1.P1.X * kraw1.P2.Y) * (kraw2.P2.Y - kraw2.P1.Y) - (kraw2.P2.X * kraw2.P1.Y - kraw2.P1.X * kraw2.P2.Y) * (kraw1.P2.Y - kraw1.P1.Y);
        }

        public bool czyDwieFigurySaProste(Figura f1, Figura f2)
        {
            if (f1.czyWielokatJestProsty() && f2.czyWielokatJestProsty())
                return true;
            else
                return false;
        }

        public bool czyDwieFigurySiePrzecinaja(Figura f1, Figura f2)
        {
            foreach (Krawedz k1 in f1.ListaKrawedzi)
                foreach (Krawedz k2 in f2.ListaKrawedzi)
                    if (Figura.czySieStykaja(k1, k2))
                        return true;
            return false;
        }



    }
}
