﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cz1
{
    class Test
    {
        GrafListowy grafListowy;
        Generator generator;
        bool czyAGwiazdka;

        public Test(GrafListowy grafListowy, bool czyAGwiazdka=false)
        {
            this.grafListowy = grafListowy;
            this.generator = new Generator(grafListowy);
            this.czyAGwiazdka = czyAGwiazdka;
        }

        public int dajRozniceOdleglosci(int p1, int p2, int kon)
        {
            return generator.dajOdleglosc(new Krawedz(p2, kon, 0)) - generator.dajOdleglosc(new Krawedz(p1, kon, 0));
        }

        

        public void Dijkstry(int pocz, int kon, int d = 2)
        {
            if (d < 2)
                Console.WriteLine("Za niska wartosc d");
            int rozm = grafListowy.Count;
            int[] odleglosc = new int[grafListowy.Count];
            int[] przodek = new int[grafListowy.Count + 1];
            bool[] odwiedzone = new bool[grafListowy.Count + 1];
            for (int i = 0; i < grafListowy.Count; i++)
            {
                odleglosc[i] = int.MaxValue;
                przodek[i] = i;
                odwiedzone[i] = false;
            }
            odleglosc[pocz] = 0;
            odwiedzone[pocz] = true;
            int licznik = 1;

            Kolejka kolejka = new Kolejka(grafListowy.Count * grafListowy.Count, d);
            int c = pocz;

            while (licznik < grafListowy.Count && c != kon)
            {
                List<int> poloczenie = grafListowy.dajPoloczenie(c);
                for (int i = 0; i < poloczenie.Count; i++)
                {
                    int aktualnaOdleglosc = odleglosc[c] + grafListowy.dajWage(c, poloczenie[i]);
                    if (aktualnaOdleglosc < odleglosc[poloczenie[i]])
                    {
                        odleglosc[poloczenie[i]] = aktualnaOdleglosc;
                        przodek[poloczenie[i]] = c;
                        if (czyAGwiazdka)
                            aktualnaOdleglosc += dajRozniceOdleglosci(c, poloczenie[i], kon);
                        if (!odwiedzone[poloczenie[i]])
                            kolejka.Push(poloczenie[i], aktualnaOdleglosc);
                    }
                }
                c = kolejka.wezDaneZPoczatkuKolejki();
                if (c == -1) break;
                odwiedzone[c] = true;
                licznik++;
            }

            List<int> sciezka = new List<int>();
            while (c != pocz && c>-1)
            {
                sciezka.Add(przodek[c]);
                c = przodek[c];
            }


        }

        public static GrafListowy dajGraf(int n, int c, int maxw)
        {
            GrafListowy grafListowy = new GrafListowy(n);
            grafListowy.tworzLosowo(c, maxw);
            return grafListowy;
        }



    }
}
