﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cz1
{
    class Kolejka
    {

        public struct ElementKolejki
        {
            public int priorytet;
            public int dane;
        }

         ElementKolejki[] elemKolejki;
         int rozmiar;
         int d;

        public Kolejka(int maksymalnyRozmiar, int d = 2)
        {
            elemKolejki = new ElementKolejki[maksymalnyRozmiar];
            rozmiar = 0;
            this.d = d;
        }


        internal ElementKolejki[] ElemKolejki
        {
            get
            {
                return elemKolejki;
            }

            set
            {
                elemKolejki = value;
            }
        }

        public int Rozmiar
        {
            get
            {
                return rozmiar;
            }

            set
            {
                rozmiar = value;
            }
        }

        public int D
        {
            get
            {
                return d;
            }

            set
            {
                d = value;
            }
        }

       

        public int wezDaneZPoczatkuKolejki()
        {
            int x = dajDaneZPoczatkuKolejki();
            Pop();
            return x;
        }

        private void Pop()
        {
            int p, q, dane, prio;

            if (Rozmiar > 0)
            {
                Rozmiar--;
                prio = ElemKolejki[Rozmiar].priorytet;
                dane = ElemKolejki[Rozmiar].dane;

                p = 0;
                q = 1;

                while (q < Rozmiar)
                {
                    q = dajMinimum(q);
                    if (prio <= ElemKolejki[q].priorytet) break;
                    ElemKolejki[p] = ElemKolejki[q];
                    p = q;
                    q = D * p + 1;
                }

                ElemKolejki[p].priorytet = prio;
                ElemKolejki[p].dane = dane;
            }
        }

        public void Push(int w, int p)
        {
            int i, j;

            i = Rozmiar++;
            j = (i - 1) / d;

            while (i > 0 && elemKolejki[j].priorytet > p)
            {
                elemKolejki[i] = elemKolejki[j];
                i = j;
                j = (i - 1) / d;
            }

            elemKolejki[i].priorytet = p;
            elemKolejki[i].dane = w;
        }


        bool czyPusta()
        {
            return rozmiar == 0 ? true : false;
        }

        private int dajMinimum(int j)
        {
            int min = j;
            for (int x = 1; x < d; x++)
                if (j + x < rozmiar && elemKolejki[j + x].priorytet < elemKolejki[min].priorytet)
                    min = j + x;
            return min;
        }

        private int dajDaneZPoczatkuKolejki()
        {
            return (Rozmiar != 0) ? ElemKolejki[0].dane : -1;
        }
    }
}
