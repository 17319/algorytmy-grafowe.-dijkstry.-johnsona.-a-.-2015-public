﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cz1
{
    class GrafListowy
    {
        List<List<int>> reprezentacjaListowaGrafu;
        List<Krawedz> listaKrawedzi;

        internal int Count
        {
            get
            {
                return reprezentacjaListowaGrafu.Count;
            }
        }


        public List<List<int>> ReprezentacjaListowaGrafu
        {
            get
            {
                return reprezentacjaListowaGrafu;
            }

            set
            {
                reprezentacjaListowaGrafu = value;
            }
        }

        internal List<Krawedz> ListaKrawedzi
        {
            get
            {
                return listaKrawedzi;
            }

            set
            {
                listaKrawedzi = value;
            }
        }

        public GrafListowy(int n)
        {
            ListaKrawedzi = new List<Krawedz>();
            ReprezentacjaListowaGrafu = new List<List<int>>();
            for (int i = 0; i < n; i++)
                ReprezentacjaListowaGrafu.Add(new List<int>());
        }

        public void dodajKrawedz(Krawedz k)
        {
            ReprezentacjaListowaGrafu[k.w1].Add(k.w2);
            ListaKrawedzi.Add(k);
        }

        public int znajdz(int v1, int v2)
        {
            if (v1 >= ReprezentacjaListowaGrafu.Count) return -1;
            for (int i = 0; i < ReprezentacjaListowaGrafu[v1].Count; i++)
            {
                if (ReprezentacjaListowaGrafu[v1][i] == v2) return i;
            }
            return -1;
        }

        int znajdzW(int v1, int v2)
        {
            for (int i = 0; i < ListaKrawedzi.Count; i++)
            {
                if (ListaKrawedzi[i].w1 == v1 && ListaKrawedzi[i].w2 == v2) return i;
            }
            return -1;
        }

        public bool czyIstniejeKrawedz(Krawedz k)
        {
            return znajdz(k.w1, k.w2) > -1 ? true : false;
        }

        public List<int> dajPoloczenie(int v)
        {
            return ReprezentacjaListowaGrafu[v];
        }

        public int dajWage(int v1, int v2)
        {
            int find = znajdzW(v1, v2);
            return find > -1 ? ListaKrawedzi[find].w : -1;
        }

        public void tworzLosowo(int c, int maksymalne_w)
        {
            int n = ReprezentacjaListowaGrafu.Count;
            int licz = c;
            while (licz > 0)
            {
                Krawedz krawedz = dajLosowaKrawedz(n, maksymalne_w + 1);
                while (czyIstniejeKrawedz(krawedz)) krawedz = dajLosowaKrawedz(n, maksymalne_w + 1);
                dodajKrawedz(krawedz);
                licz--;
            }
        }

        public Krawedz dajLosowaKrawedz(int n, int w)
        {
            int pocz = 0, kon = n, v1, v2;
            Random rand;
            rand = new Random();
            v1 = pocz < kon ? pocz + rand.Next() % (kon - pocz) : pocz;
            rand = new Random();
            v2 = pocz < kon ? pocz + rand.Next() % (kon - pocz) : pocz;
            while (v2 == v1)
                v2 = pocz < kon ? pocz + rand.Next() % (kon - pocz) : pocz;
            pocz = 1;
            kon = w;
            return new Krawedz(v1, v2, pocz < kon ? pocz + rand.Next() % (kon - pocz) : pocz);
        }
    }
}
