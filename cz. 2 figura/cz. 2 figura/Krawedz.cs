﻿namespace cz._2_figura
{
    class Krawedz
    {
        Punkt p1, p2;

        public Krawedz(Punkt p1, Punkt p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        internal int DajLewaGranice
        {
            get
            {
                if (p1.X < p2.X)
                    return p1.X;
                else
                    return p2.X;
            }
        }
        internal int DajPrawaGranice
        {
            get
            {
                if (p1.X > p2.X)
                    return p1.X;
                else
                    return p2.X;
            }
        }
        public int DajGornaGranice
        {
            get
            {
                if (p1.Y > p2.Y)
                    return p1.Y;
                else
                    return p2.Y;
            }
        }
        public int DajDolnaGranice
        {
            get
            {
                if (p1.Y < p2.Y)
                    return p1.Y;
                else
                    return p2.Y;
            }
        }

        internal Punkt P1
        {
            get
            {
                return p1;
            }

            set
            {
                p1 = value;
            }
        }

        internal Punkt P2
        {
            get
            {
                return p2;
            }

            set
            {
                p2 = value;
            }
        }

    }
}
