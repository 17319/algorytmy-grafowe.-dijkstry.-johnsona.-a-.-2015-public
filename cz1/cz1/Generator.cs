﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cz1
{
    class Generator
    {


        List<Point> listaMiast;
        GrafListowy grafListowy;

        public Generator(GrafListowy grafListowy)
        {
            this.grafListowy = grafListowy;
            listaMiast = new List<Point>();
            for (int i = 0; i < grafListowy.ReprezentacjaListowaGrafu.Count; i++)
            {
                for (int j = 0; j < grafListowy.ReprezentacjaListowaGrafu[i].Count; j++)
                {
                    listaMiast.Add(new Point(i,j));
                }
            }
        }

        public int Count()
        {
            return grafListowy.Count;
        }


        public int dajOdleglosc(Krawedz kraw)
        {
            double a = listaMiast[kraw.w1].X - listaMiast[kraw.w2].X;
            double b = listaMiast[kraw.w1].Y - listaMiast[kraw.w2].Y;
            return (int)Math.Sqrt(a * a + b * b);
        }


    }
}
